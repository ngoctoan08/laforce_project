<?php
require_once './configs/config_vnpay.php';
require_once './Models/Cart.php';
require_once './Core/Mailer.php';

echo '<pre>';
    print_r($_POST);
    // print_r($_SESSION['cart']);
    echo '</pre>';
if(isset($_POST['redirect'])) {
    ## Thông tin khách hàng
    $name = $_POST['name'];
    $email = $_POST['email'];
    $phone = $_POST['phone'];
    $address = $_POST['address'];
    $note = $_POST['note'];
    $payment = $_POST['payment'];

    // mã đơn hàng là một số ngẫu nhiên
    $idOrder = rand(0, 9999);
    $_SESSION['order_id'] = $idOrder;
    $error = [];
    if($payment == "vnpay") {
        $totalCheckOut = 0;
            foreach($_SESSION['cart'] as $id) {
                foreach($id as $product) {
                    $total = $product['qty'] * $product['price'];
                    $totalCheckOut += $total;
                }
            }
        $vnp_TxnRef = $idOrder; //Mã đơn hàng. Trong thực tế Merchant cần insert đơn hàng vào DB và gửi mã này sang VNPAY
        $vnp_OrderInfo = "Thanh toán tại website";
        $vnp_OrderType = 'billpayment';
        $vnp_Amount = $totalCheckOut * 100;
        $vnp_Locale = 'vn';
        $vnp_BankCode = 'NCB';
        $vnp_IpAddr = $_SERVER['REMOTE_ADDR'];
        //Add Params of 2.0.1 Version
        $vnp_ExpireDate = $expire;

        $inputData = array(
            "vnp_Version" => "2.1.0",
            "vnp_TmnCode" => $vnp_TmnCode,
            "vnp_Amount" => $vnp_Amount,
            "vnp_Command" => "pay",
            "vnp_CreateDate" => date('YmdHis'),
            "vnp_CurrCode" => "VND",
            "vnp_IpAddr" => $vnp_IpAddr,
            "vnp_Locale" => $vnp_Locale,
            "vnp_OrderInfo" => $vnp_OrderInfo,
            "vnp_OrderType" => $vnp_OrderType,
            "vnp_ReturnUrl" => $vnp_Returnurl,
            "vnp_TxnRef" => $vnp_TxnRef,
            "vnp_ExpireDate"=>$vnp_ExpireDate
        );

        if (isset($vnp_BankCode) && $vnp_BankCode != "") {
            $inputData['vnp_BankCode'] = $vnp_BankCode;
        }

        //var_dump($inputData);
        ksort($inputData);
        $query = "";
        $i = 0;
        $hashdata = "";
        foreach ($inputData as $key => $value) {
            if ($i == 1) {
                $hashdata .= '&' . urlencode($key) . "=" . urlencode($value);
            } else {
                $hashdata .= urlencode($key) . "=" . urlencode($value);
                $i = 1;
            }
            $query .= urlencode($key) . "=" . urlencode($value) . '&';
        }

        $vnp_Url = $vnp_Url . "?" . $query;
        if (isset($vnp_HashSecret)) {
            $vnpSecureHash =   hash_hmac('sha512', $hashdata, $vnp_HashSecret);//  
            $vnp_Url .= 'vnp_SecureHash=' . $vnpSecureHash;
        }
        $returnData = array('code' => '00'
            , 'message' => 'success'
            , 'data' => $vnp_Url);
        if (isset($_POST['redirect'])) { //CLick đặt hàng, sẽ gửi mail trước rồi thanh toán sau.

            // $_SESSION['order_id'] = $idOrder;
            $cartModel = new Cart();
            $createCustomer = $cartModel->insertCustomer($name, $email, $phone, $address);
            $idCustomer = $cartModel->returnLastId(); //Dùng để insert vào bảng order
            $createOrder = $cartModel->insertOrder($idOrder, $idCustomer, $note, $payment); //Thêm thông tin vào bảng Order

            if(isset($_SESSION['cart']) || !empty($_SESSION['cart'])) {
                // Nội dung gửi mail là một table bao gồm các trường thông tin như dưới
                $content = "<h3 style='font-size: 0.9em; font-family: sans-serif; padding: 12px 15px;'> Cảm ơn quý khách đã đặt hàng tại Laforce </h3>";
                $content .= "<h3 style='font-size: 0.9em; font-family: sans-serif; padding: 12px 15px;'> Mã đơn hàng: ".$idOrder."</h3>";
                $content .= "<table style='
                    border-collapse: collapse;
                    margin: 25px 0;
                    font-size: 0.9em;
                    font-family: sans-serif;
                    min-width: 800px;
                    box-shadow: 0 0 20px rgba(0, 0, 0, 0.15);
                    text-align: center;'>
                            <thead>
                                <tr style='background-color: #009879;
                                color: #ffffff;
                                text-align: center;'>
                                    <th style='padding: 12px 15px;'> STT</th>
                                    <th> Tên sản phẩm</th>
                                    <th> Size</th>
                                    <th> Đơn giá</th>
                                    <th> SL</th>
                                    <th> Tổng tiền</th>
                                </tr>
                        </thead>
                        <tbody>
                        ";
                $count = 0;
                $totalCheckOut = 0;
                foreach($_SESSION['cart'] as $id) {
                    foreach($id as $product) {
                        $total = $product['qty'] * $product['price'];
                        $totalCheckOut += $total;
                        $count ++;
                        $content.="
                            <tr style='font-weight: bold;
                            color: #009879;'>
                                <td style='padding: 12px 15px;'>".$count."</td>
                                <td>".$product['name']."</td>
                                <td>".$product['size']."</td>
                                <td>".currency_format($product['price'])."</td>
                                <td>".$product['qty']."</td>
                                <td>".currency_format($total)."</td>
                            </tr>";
                        $createOrderDetail = $this->cartModel->insertOrderDetail($idOrder, $product['id'], $product['name'], $product['size'], $product['price'], $product['qty'], $total);
                    }
                }
                $content.="</tbody> </table>" ;      
                $content.= "<h3 style='font-size: 0.9em; font-family: sans-serif; padding: 12px 15px;'> Tổng tiền thanh toán: <span style='color: red;'>".currency_format($totalCheckOut)."</span></h3>";

                $content.= "<table style='
                border-collapse: collapse;
                    margin: 25px 0;
                    font-size: 0.9em;
                    font-family: sans-serif;
                    min-width: 800px;
                    box-shadow: 0 0 20px rgba(0, 0, 0, 0.15); 
                    text-align:center'>
                        <tr style='font-weight: bold; text-align:center'>
                            <td style='padding: 12px 15px; '>Khách hàng: </td>
                            <td>".$name."</td>
                        </tr>
                        <tr style='font-weight: bold; text-align:center'>
                            <td style='padding: 12px 15px; '>Địa chỉ giao hàng: </td>
                            <td>".$address."</td>
                        </tr>
                        <tr style='font-weight: bold; text-align:center'>
                            <td style='padding: 12px 15px; '>Số điện thoại: </td>
                            <td>".$phone."</td>
                        </tr>
                        <tr style='font-weight: bold; text-align:center'>
                            <td style='padding: 12px 15px;'>Email</td>
                            <td>".$email."</td>
                        </tr>
                        <tr style='font-weight: bold; text-align:center'>
                            <td style='padding: 12px 15px;'>Phương thức thanh toán: </td>
                            <td>".payment($payment)."</td>
                        </tr>
                </table>";
                $title = "Bạn đã mua hàng thành công tại Laforce";
            }
            
            if($createOrderDetail) {
                // Gửi mail
                $mail = new Mailer();
                $mail -> sendMail($email, $name, $title, $content);
                // unset($_SESSION['cart']);
                $_SESSION['info'] = [$name, $address, $phone, $email, $payment];
                // header('location: san-pham/giay-tay.html');
            }
            header('Location: ' . $vnp_Url);
            die();
        } else {
            echo json_encode($returnData);
        }

    }

    elseif($payment == "code") {
        $cartModel = new Cart();
        $createCustomer = $cartModel->insertCustomer($name, $email, $phone, $address);
        $idCustomer = $cartModel->returnLastId();
        $createOrder = $cartModel->insertOrder($idOrder, $idCustomer, $note, $payment);
        if(isset($_SESSION['cart']) || !empty($_SESSION['cart'])) {
            // Nội dung gửi mail là một table bao gồm các trường thông tin như dưới
            $content = "<h3 style='font-size: 0.9em; font-family: sans-serif; padding: 12px 15px;'> Cảm ơn quý khách đã đặt hàng tại Laforce </h3>";
            $content .= "<h3 style='font-size: 0.9em; font-family: sans-serif; padding: 12px 15px;'> Mã đơn hàng: ".$idOrder."</h3>";
            $content .= "<table style='
                border-collapse: collapse;
                margin: 25px 0;
                font-size: 0.9em;
                font-family: sans-serif;
                min-width: 800px;
                box-shadow: 0 0 20px rgba(0, 0, 0, 0.15);
                text-align: center;'>
                        <thead>
                            <tr style='background-color: #009879;
                            color: #ffffff;
                            text-align: center;'>
                                <th style='padding: 12px 15px;'> STT</th>
                                <th> Tên sản phẩm</th>
                                <th> Size</th>
                                <th> Đơn giá</th>
                                <th> SL</th>
                                <th> Tổng tiền</th>
                            </tr>
                    </thead>
                    <tbody>
                    ";
            $count = 0;
            $totalCheckOut = 0;
            foreach($_SESSION['cart'] as $id) {
                foreach($id as $product) {
                    $total = $product['qty'] * $product['price'];
                    $totalCheckOut += $total;
                    $count ++;
                    $content.="
                        <tr style='font-weight: bold;
                        color: #009879;'>
                            <td style='padding: 12px 15px;'>".$count."</td>
                            <td>".$product['name']."</td>
                            <td>".$product['size']."</td>
                            <td>".currency_format($product['price'])."</td>
                            <td>".$product['qty']."</td>
                            <td>".currency_format($total)."</td>
                        </tr>";
                    $createOrderDetail = $cartModel->insertOrderDetail($idOrder, $product['id'], $product['name'], $product['size'], $product['price'], $product['qty'], $total);
                }
            }
            $content.="</tbody> </table>" ;      
            $content.= "<h3 style='font-size: 0.9em; font-family: sans-serif; padding: 12px 15px;'> Tổng tiền thanh toán: <span style='color: red;'>".currency_format($totalCheckOut)."</span></h3>";

            $content.= "<table style='
            border-collapse: collapse;
                margin: 25px 0;
                font-size: 0.9em;
                font-family: sans-serif;
                min-width: 800px;
                box-shadow: 0 0 20px rgba(0, 0, 0, 0.15); 
                text-align:center'>
                    <tr style='font-weight: bold; text-align:center'>
                        <td style='padding: 12px 15px; '>Khách hàng: </td>
                        <td>".$name."</td>
                    </tr>
                    <tr style='font-weight: bold; text-align:center'>
                        <td style='padding: 12px 15px; '>Địa chỉ giao hàng: </td>
                        <td>".$address."</td>
                    </tr>
                    <tr style='font-weight: bold; text-align:center'>
                        <td style='padding: 12px 15px; '>Số điện thoại: </td>
                        <td>".$phone."</td>
                    </tr>
                    <tr style='font-weight: bold; text-align:center'>
                        <td style='padding: 12px 15px;'>Email: </td>
                        <td>".$email."</td>
                    </tr>
                    <tr style='font-weight: bold; text-align:center'>
                        <td style='padding: 12px 15px;'>Phương thức thanh toán: </td>
                        <td>".payment($payment)."</td>
                    </tr>
            </table>";
            $title = "Bạn đã mua hàng thành công tại Laforce";
        }
        if($createOrderDetail) {
            // Gửi mail
            // $mail = new Mailer();
            // $mail -> sendMail($email, $name, $title, $content);
            $_SESSION['info'] = [$name, $address, $phone, $email, $payment];
            header('location: success.html');
        }
    }

    else {
        echo "toan";
    }
}

