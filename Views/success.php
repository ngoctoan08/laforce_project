<?php
include_once './Models/Vnpay.php';
if(isset($_GET['vnp_Amount']) && isset($_SESSION['order_id'])) {
    $vnp_Amount= $_GET['vnp_Amount'];
    $vnp_BankCode = $_GET['vnp_BankCode'];
    $vnp_BankTranNo = $_GET['vnp_BankTranNo'];
    $vnp_CardType = $_GET['vnp_CardType'];
    $vnp_OrderInfo = $_GET['vnp_OrderInfo'];
    $vnp_PayDate = $_GET['vnp_PayDate'];
    $vnp_ResponseCode = $_GET['vnp_ResponseCode'];
    $vnp_TmnCode = $_GET['vnp_TmnCode'];
    $vnp_TransactionNo = $_GET['vnp_TransactionNo'];
    $idOrder = $_SESSION['order_id'];

    // insert vào database table vnpay
    $vnpay = new Vnpay();
    $add = $vnpay->insertVnpay($vnp_Amount, $vnp_BankCode, $vnp_BankTranNo, $vnp_CardType, $vnp_OrderInfo, $vnp_PayDate, $vnp_ResponseCode, $vnp_TmnCode, $vnp_TransactionNo, $idOrder);

    if($add)
    {   
        $name = $_SESSION['info'][0];
        $address = $_SESSION['info'][1];
        $phone = $_SESSION['info'][2];
        $email = $_SESSION['info'][3];
        $payment = $_SESSION['info'][4];
    
        $idOrder = $_SESSION['order_id'];
        // Nội dung gửi mail là một table bao gồm các trường thông tin như dưới
        // $content = "<h3 style='font-size: 0.9em; font-family: sans-serif; padding: 12px 15px;'> Cảm ơn quý khách đã đặt hàng tại Laforce </h3>";
        $content = "<h3 style='font-size: 0.9em; font-family: sans-serif; padding: 12px 15px;'> Mã đơn hàng: ".$idOrder."</h3>";
        $content .= "<table style='
            border-collapse: collapse;
            margin: 25px 0;
            font-size: 0.9em;
            font-family: sans-serif;
            min-width: 800px;
            box-shadow: 0 0 20px rgba(0, 0, 0, 0.15);
            text-align: center;'>
                    <thead>
                        <tr style='background-color: #009879;
                        color: #ffffff;
                        text-align: center;'>
                            <th style='padding: 12px 15px; text-align: center;'> STT</th>
                            <th> Tên sản phẩm</th>
                            <th> Size</th>
                            <th> Đơn giá</th>
                            <th> SL</th>
                            <th> Tổng tiền</th>
                        </tr>
                </thead>
                <tbody>
                ";
        $count = 0;
        $totalCheckOut = 0;
        foreach($_SESSION['cart'] as $id) {
            foreach($id as $product) {
                $total = $product['qty'] * $product['price'];
                $totalCheckOut += $total;
                $count ++;
                $content.="
                    <tr style='font-weight: bold;
                    color: #009879;'>
                        <td style='padding: 12px 15px;'>".$count."</td>
                        <td>".$product['name']."</td>
                        <td>".$product['size']."</td>
                        <td>".currency_format($product['price'])."</td>
                        <td>".$product['qty']."</td>
                        <td>".currency_format($total)."</td>
                    </tr>";
            }
        }
        $content.="</tbody> </table>" ;      
        $content.= "<h3 style='font-size: 0.9em; font-family: sans-serif; padding: 12px 15px;'> Tổng tiền thanh toán: <span style='color: red;'>".currency_format($totalCheckOut)."</span></h3>";
    
        $content.= "<table style='
        border-collapse: collapse;
            margin: 25px 0;
            font-size: 0.9em;
            font-family: sans-serif;
            min-width: 800px;
            box-shadow: 0 0 20px rgba(0, 0, 0, 0.15); 
            text-align:center'>
                <tr style='font-weight: bold; text-align:center'>
                    <td style='padding: 12px 15px; '>Khách hàng: </td>
                    <td>".$name."</td>
                </tr>
                <tr style='font-weight: bold; text-align:center'>
                    <td style='padding: 12px 15px; '>Địa chỉ giao hàng: </td>
                    <td>".$address."</td>
                </tr>
                <tr style='font-weight: bold; text-align:center'>
                    <td style='padding: 12px 15px; '>Số điện thoại: </td>
                    <td>".$phone."</td>
                </tr>
                <tr style='font-weight: bold; text-align:center'>
                    <td style='padding: 12px 15px;'>Email</td>
                    <td>".$email."</td>
                </tr>
                <tr style='font-weight: bold; text-align:center'>
                    <td style='padding: 12px 15px;'>Phương thức thanh toán: </td>
                    <td>".payment($payment)."</td>
                </tr>
        </table>";
        ?>
        <div class="container">
            <?php echo $content;?>
        </div>
        <?php
        unset($_SESSION['order_id']);
        unset($_SESSION['info']);
        unset($_SESSION['cart']);
    }
    else {
        echo "thanh toán thất bại!";
    }
}

if(isset($_SESSION['cart']) || !empty($_SESSION['cart'])) {
    //tHÔNG TIN KHÁCH HÀNG
    $name = $_SESSION['info'][0];
    $address = $_SESSION['info'][1];
    $phone = $_SESSION['info'][2];
    $email = $_SESSION['info'][3];
    $payment = $_SESSION['info'][4];

    $idOrder = $_SESSION['order_id'];
    // Nội dung gửi mail là một table bao gồm các trường thông tin như dưới
    // $content = "<h3 style='font-size: 0.9em; font-family: sans-serif; padding: 12px 15px;'> Cảm ơn quý khách đã đặt hàng tại Laforce </h3>";
    $content = "<h3 style='font-size: 0.9em; font-family: sans-serif; padding: 12px 15px;'> Mã đơn hàng: ".$idOrder."</h3>";
    $content .= "<table style='
        border-collapse: collapse;
        margin: 25px 0;
        font-size: 0.9em;
        font-family: sans-serif;
        min-width: 800px;
        box-shadow: 0 0 20px rgba(0, 0, 0, 0.15);
        text-align: center;'>
                <thead>
                    <tr style='background-color: #009879;
                    color: #ffffff;
                    text-align: center;'>
                        <th style='padding: 12px 15px; text-align: center;'> STT</th>
                        <th> Tên sản phẩm</th>
                        <th> Size</th>
                        <th> Đơn giá</th>
                        <th> SL</th>
                        <th> Tổng tiền</th>
                    </tr>
            </thead>
            <tbody>
            ";
    $count = 0;
    $totalCheckOut = 0;
    foreach($_SESSION['cart'] as $id) {
        foreach($id as $product) {
            $total = $product['qty'] * $product['price'];
            $totalCheckOut += $total;
            $count ++;
            $content.="
                <tr style='font-weight: bold;
                color: #009879;'>
                    <td style='padding: 12px 15px;'>".$count."</td>
                    <td>".$product['name']."</td>
                    <td>".$product['size']."</td>
                    <td>".currency_format($product['price'])."</td>
                    <td>".$product['qty']."</td>
                    <td>".currency_format($total)."</td>
                </tr>";
        }
    }
    $content.="</tbody> </table>" ;      
    $content.= "<h3 style='font-size: 0.9em; font-family: sans-serif; padding: 12px 15px;'> Tổng tiền thanh toán: <span style='color: red;'>".currency_format($totalCheckOut)."</span></h3>";

    $content.= "<table style='
    border-collapse: collapse;
        margin: 25px 0;
        font-size: 0.9em;
        font-family: sans-serif;
        min-width: 800px;
        box-shadow: 0 0 20px rgba(0, 0, 0, 0.15); 
        text-align:center'>
            <tr style='font-weight: bold; text-align:center'>
                <td style='padding: 12px 15px; '>Khách hàng: </td>
                <td>".$name."</td>
            </tr>
            <tr style='font-weight: bold; text-align:center'>
                <td style='padding: 12px 15px; '>Địa chỉ giao hàng: </td>
                <td>".$address."</td>
            </tr>
            <tr style='font-weight: bold; text-align:center'>
                <td style='padding: 12px 15px; '>Số điện thoại: </td>
                <td>".$phone."</td>
            </tr>
            <tr style='font-weight: bold; text-align:center'>
                <td style='padding: 12px 15px;'>Email</td>
                <td>".$email."</td>
            </tr>
            <tr style='font-weight: bold; text-align:center'>
                <td style='padding: 12px 15px;'>Phương thức thanh toán: </td>
                <td>".payment($payment)."</td>
            </tr>
    </table>";
    $content .= '<a> '
    ?>
    <div class="container">
        <?php echo $content;?>
    </div>
    <?php
    unset($_SESSION['order_id']);
    unset($_SESSION['info']);
    unset($_SESSION['cart']);

}