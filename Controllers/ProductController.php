<?php
include_once './Core/Controller.php';
class ProductController extends Controller
{
    public $productModel;
    

    public function __construct()
    {
        $this->productModel = parent::model('Product');
        // $this->sizeModel = parent::model('Size');
        $this->index();
        // $this->viewProduct();
    }

    public function index()
    {
        $method = '';
        if(isset($_GET['method'])) {
            $method = $_GET['method'];
        }
        switch($method) {
            case 'western';
                $nav = isset($_GET['nav']) ? $_GET['nav'] : 1;
                $categoryId = 1;
                $showSizes = $this->productModel->showSize();
                $countProduct = count($this->productModel->countProductByCateId($categoryId));
                $row = 9; //Số sản phẩm lấy ra
                $from = ($nav - 1) * $row; // lấy lần lượt từng bản ghi
                $countPage = ceil($countProduct/$row); // Số trang
                $products = $this->productModel->showProductByCateId($categoryId, $from, $row);
                foreach($products as $product) {
                    $sizes[$product['id']] = $this->productModel->showSizeById($product['id']);
                }
                include_once './Views/product.php';
                break;
            
            case 'lazy':
                $categoryId = 2;
                $nav = isset($_GET['nav']) ? $_GET['nav'] : 1;
                $showSizes = $this->productModel->showSize();
                $countProduct = count($this->productModel->countProductByCateId($categoryId));
                $row = 9; //Số sản phẩm lấy ra
                $from = ($nav - 1) * $row; // lấy lần lượt từng bản ghi
                $countPage = ceil($countProduct/$row); // Số trang
                $products = $this->productModel->showProductByCateId($categoryId, $from, $row);
                foreach($products as $product) {
                    $sizes[$product['id']] = $this->productModel->showSizeById($product['id']);
                }
                include_once './Views/product.php';
                break;
            }

        if(isset($_GET['id'])) {
            $id = $_GET['id'];
            $product = $this->productModel->showProductById($id);
            $listImg = $this->productModel->showImgProductById($id);
            $sizes[$product['id']] = $this->productModel->showSizeById($product['id']);
            $feedbacks = $this->productModel->showFeedback($id);
            $arrange = 0;
            foreach($feedbacks as $feedback) {
                $arrange += $feedback['rate'];
            }

            // Viết feedback
                if(isset($_POST['btn-feedback'])) {
                    $id = $_GET['id'];
                    $name = $_POST['name'];
                    $phone = $_POST['phone'];
                    $rate = $_POST['rate'];
                    $comment = $_POST['comment'];
                    $items = $this->productModel->checkFeedback($phone);
                    $flag = 0;
                    foreach($items as $item) {
                        if($id == $item['product_id']) {
                            $flag = 1;     
                        }
                    }
                    echo $flag;
                    if($flag == 1) {
                        $add = $this->productModel->addFeedback($id, $name, $phone, $rate, $comment);
                        header('location: san-pham/'.$id.'/'.converSlugUrl($product['name']).'.html');
                        // header('location: index.php?page=product&id='.$id.'&name='.$product['name']);
                    }
                    else {
                        $error['noti'] = "Bạn không gửi được feedback do chưa mua hàng!";
                    }
                }
            include_once './Views/detail_product.php';
        }
    }

    public function viewProduct()
    {
        if(isset($_GET['id'])) {
            $id = $_GET['id'];
            $product = $this->productModel->showProductById($id);
            $listImg = $this->productModel->showImgProductById($id);
            $sizes[$product['id']] = $this->productModel->showSizeById($product['id']);
            include_once './Views/detail_product.php';
        }
    }

    public function feedback($id)
    {
                
                include_once './Views/detail_product.php';
    }
}